package message;

import controller.DatabaseController;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.naming.NamingException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Dmitriy on 18.04.2016.
 */
@MessageDriven(name = "MessageBean", activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/TestQ"),
        @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge")})
public class MessageBean implements MessageListener {

    @Override
    public void onMessage(Message message) {
        DatabaseController databaseController = new DatabaseController();
        try {
            Timestamp timestamp = new Timestamp(message.getJMSTimestamp());
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(timestamp.getTime());
            SimpleDateFormat formatDate = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss:S");
            if (message instanceof TextMessage) {
                TextMessage textMessage = (TextMessage) message;
                databaseController.insert(new model.Message(textMessage.getText(), formatDate.format(calendar.getTime())));
            } else {
                databaseController.insert(new model.Message("Wrong message type!", formatDate.format(calendar.getTime())));
            }
        } catch (NamingException | SQLException | JMSException e) {
            e.printStackTrace();
        }
    }
}
