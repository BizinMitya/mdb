package model;

import java.io.Serializable;

/**
 * Created by Dmitriy on 08.05.2016.
 */
public class Message implements Serializable {
    private String message;
    private String time;

    public Message() {

    }

    public Message(String message, String time) {
        this.setMessage(message);
        this.setTime(time);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
