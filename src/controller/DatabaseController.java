package controller;

import model.Message;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dmitriy on 08.05.2016.
 */
public class DatabaseController {
    public static final String INSERT = "INSERT INTO `netcracker`.`mdb` (`message`, `time`) VALUES (?, ?)";
    public static final String SELECT = "SELECT message, time FROM netcracker.mdb WHERE STR_TO_DATE(time, '%d.%m.%Y %H:%i:%s:%f') >= STR_TO_DATE(?, '%d.%m.%Y %H:%i:%s:%f') AND STR_TO_DATE(time, '%d.%m.%Y %H:%i:%s:%f') <= STR_TO_DATE(?, '%d.%m.%Y %H:%i:%s:%f')";

    private Connection getConnection() throws SQLException, NamingException {
        return ((DataSource) new InitialContext().lookup("java:/MySqlDS")).getConnection("root", "jExonyz6");
    }

    public void insert(Message message) throws NamingException, SQLException {
        try (Connection connection = getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT);
            preparedStatement.setString(1, message.getMessage());
            preparedStatement.setString(2, message.getTime());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Message> select(String date1, String date2) throws SQLException {
        List<Message> messageList = null;
        try (Connection connection = getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT);
            preparedStatement.setString(1, date1);
            preparedStatement.setString(2, date2);
            ResultSet resultSet = preparedStatement.executeQuery();
            messageList = new ArrayList<>();
            while (resultSet.next()) {
                messageList.add(new Message(resultSet.getString("message"), resultSet.getString("time")));
            }
        } catch (NamingException e) {
            e.printStackTrace();
        }
        return messageList;
    }
}
