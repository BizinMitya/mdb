<%@ page import="javax.jms.*" %>
<%@ page import="javax.naming.Context" %>
<%@ page import="javax.naming.InitialContext" %>
<%@ page import="javax.naming.NamingException" %>
<%--
  Created by IntelliJ IDEA.
  User: Dmitriy
  Date: 07.05.2016
  Time: 23:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page errorPage="error.jsp" contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Отправка сообщения</title>
    <link rel="stylesheet" type="text/css" href="bootstrap-3.3.6-dist/css/bootstrap.css">
</head>
<body>
<form class="form-inline" action="index.jsp" method="post">
    <br>Введите сообщение: <input id="message" name="message">
    <input name="send" type="submit" value="Отправить">
</form>
<a href="messages.jsp">Показать сообщения</a><br>
<%!
    public static final String destinationName = "java:/jboss/exported/jms/queue/TestQ";
%>
<%
    request.setCharacterEncoding("UTF-8");
    ConnectionFactory connectionFactory = (ConnectionFactory) session.getAttribute("connectionFactory");
    Queue queue = (Queue) session.getAttribute("queue");
    Connection connection = (Connection) session.getAttribute("connection");
    Session jmsSession = (Session) session.getAttribute("jmsSession");
    MessageProducer messageProducer = (MessageProducer) session.getAttribute("messageProducer");
    if (connectionFactory == null || queue == null || connection == null || jmsSession == null || messageProducer == null) {
        try {
            Context context = new InitialContext();
            connectionFactory = (ConnectionFactory) context.lookup("/ConnectionFactory");
            session.setAttribute("connectionFactory", connectionFactory);
            queue = (Queue) context.lookup(destinationName);
            session.setAttribute("queue", queue);
            connection = connectionFactory.createConnection();
            session.setAttribute("connection", connection);
            jmsSession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            session.setAttribute("jmsSession", jmsSession);
            messageProducer = jmsSession.createProducer(queue);
            session.setAttribute("messageProducer", messageProducer);
        } catch (JMSException | NamingException e) {
            e.printStackTrace();
            throw new Exception("Ошибка создания очереди!");
        }
    }
    if (request.getMethod().equalsIgnoreCase("post")) {
        String msg = request.getParameter("message");
        try {
            connection.start();
            Message message = jmsSession.createTextMessage(msg);
            messageProducer.send(message);
            out.println("Сообщение отправлено!");
        } catch (JMSException e) {
            e.printStackTrace();
            throw new Exception("Ошибка отправки сообщения!");
        }
    }
%>
</body>
</html>
