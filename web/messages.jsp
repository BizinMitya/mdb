<%@ page import="controller.DatabaseController" %>
<%@ page import="model.Message" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: Dmitriy
  Date: 08.05.2016
  Time: 14:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page errorPage="error.jsp" contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Сообщения</title>
    <link rel="stylesheet" type="text/css" href="bootstrap-3.3.6-dist/css/bootstrap.css">
</head>
<body>
<a href="index.jsp">На главную</a><br>
Формат даты: dd.mm.YYYY HH:ii:ss:fff<br>
Пример даты: 01.01.1970 00:00:00:000<br>
<form class="form-inline" action="messages.jsp" method="post">
    Введите первую дату: <input id="date1" name="date1"><br><br>
    Введите вторую дату: <input id="date2" name="date2"><br><br>
    <input name="show" type="submit" value="Показать">
</form>
<%
    request.setCharacterEncoding("UTF-8");
    if (request.getMethod().equalsIgnoreCase("post")) {
        DatabaseController databaseController = new DatabaseController();
        String date1 = request.getParameter("date1");
        String date2 = request.getParameter("date2");
        try {
            List<Message> messageList = databaseController.select(date1, date2);
            out.println("<center><table class=\"table table-bordered\"><tr><th>Сообщение</th><th>Дата</th></tr>");
            for (Message message : messageList) {
                out.println("<tr><td>" + message.getMessage() + "</td><td>" + message.getTime() + "</td></tr>");
            }
            out.println("</table></center>");
        } catch (SQLException e) {
            e.printStackTrace();
            throw new Exception("Неправильный формат даты!");
        }
    }
%>
</body>
</html>
