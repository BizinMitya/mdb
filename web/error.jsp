<%--
  Created by IntelliJ IDEA.
  User: Dmitriy
  Date: 08.05.2016
  Time: 14:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isErrorPage="true" %>
<html>
<head>
    <title>Ошибка</title>
    <link rel="stylesheet" type="text/css" href="bootstrap-3.3.6-dist/css/bootstrap.css">
</head>
<body>
<%
    out.println(exception.getMessage());
%>
Вернуться на <a href="index.jsp">главную</a>
</body>
</html>
